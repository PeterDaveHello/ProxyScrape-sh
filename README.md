# ProxyScrape.sh

Scrape SOCKS5 proxy list from multiple sources, and test them with curl to see if they work.

A proxy working for me, may not work for you, because of the geo location, network routing, or the Internet quality. Test the proxy from your location, by yourself, could be the most accurate way.

This script also supports to test the proxies using specific target, as different target may also have different availavility result, not just the reasons mentioned above, the other things like IP reputation to the target services that using different WAF or NGW, will also affect that if you can access a certain target from a certain proxy, so it's best to test the proxy and target on your own.

## License

GPL-2.0 (GNU GENERAL PUBLIC LICENSE Version 2)
